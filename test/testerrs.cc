/* vim: set cin ts=4 sw=4 tw=80: */
// testcols.cc
//
// Copyright (C) 2014 - Atri Bhattacharya
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include <functional>
#include "../include/cpplotter.hpp"

using namespace std;
using namespace plotopts;

double ripples(const double & x)
{
	return 0.05 * sin( 5 * M_PI * (x - 1) );
}

int main(int argc, char const * argv[])
{
	const unsigned int  NPTS = 100;
	vecd x1, y1, y2;
	for (unsigned int i = 0; i <= NPTS; i += 1)
	{
		x1.push_back(1 + 2 * static_cast<float>(i) / NPTS);
	}
	arrd2 xlims = {x1.front(), x1.back()};
	cpplcanvas can(xlims, {0, 1}, 0);
	cpplplot p(can, argc, argv, PL_PARSE_SKIP);
	p.setlabels ("x", "y");

	y1 = x1; y2.resize(x1.size());
	transform(y1.begin(), y1.end(), y1.begin(), ripples);
	const unsigned int NLINES = 8;
	float incre = 1.0 / static_cast<float>(NLINES+1);
	for (unsigned int i = 1; i <= NLINES; i++)
	{
		fill(y2.begin(), y2.end(), incre);
		transform(y1.begin(), y1.end(), y2.begin(), y1.begin(), plus<double>());

		vecd yerrm(y1.size(), 0.05), yerrp(y1.size(), 0.05);
		transform(y1.begin(), y1.end(),
		          yerrm.begin(), yerrm.begin(), minus<double>());
		transform(y1.begin(), y1.end(),
		          yerrp.begin(), yerrp.begin(), plus<double>());
		ptsty ptdot(PTGLYPH["Dot"], cpplrgbcol(1), -1, 1);
		if (i%2)
			p.ptgraph(x1, y1, _plstyle=ptdot, _thindata=3,
			          _yerrdata=boost::make_tuple(yerrm, yerrp));
		else
			p.ptgraph(x1, y1, _plstyle=ptdot, _thindata=3);	
		p.linegraph(x1, y1, _nametag=linesty::ColName());
	}

	return 0;
}

