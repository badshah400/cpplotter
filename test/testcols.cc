/* vim: set cin ts=4 sw=4 tw=80: */
// testcols.cc
//
// Copyright (C) 2014 - Atri Bhattacharya
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include "../include/cpplotter.hpp"

using namespace std;
using namespace plotopts;

int main(int argc, char const * argv[])
{
	const unsigned int  NPTS = 11;
	vecd x1, y1, y2;
	for (unsigned int i = 0; i <= NPTS; i += 1)
	{
		x1.push_back(1 + 2 * static_cast<float>(i) / NPTS);
	}
	arrd2 xlims = {x1.front(), x1.back()};
	cpplcanvas can(xlims, {0, 1}, 0);
	cpplplot p(can, argc, argv, PL_PARSE_SKIP);
	p.setlabels ("x", "y");

	y1.resize(x1.size());
	y2.resize(x1.size());
	const unsigned int NLINES = 20;
	float yval = 0.0, incre = 1.0 / static_cast<float>(NLINES+1);
	for (unsigned int i = 1; i <= NLINES; i++)
	{
		fill(y2.begin(), y2.end(), yval);
		fill(y1.begin(), y1.end(), yval+=incre);
		p.linegraph(x1, y1, _nametag=linesty::ColName());
		p.ptgraph  (x1, y1);
		p.shadec2c (x1, y1, y2, fillsty(cpplrgbcol(0.3), 0, 0));
	}
	fill(y2.begin(), y2.end(), yval+=incre);
	p.shadec2c (x1, y1, y2, fillsty(cpplrgbcol(0.3), 0, 0));

	p.draw_legend(_leg_ncols=2, _leg_pos=PL_POSITION_INSIDE|PL_POSITION_SUBPAGE);
	return 0;
}

