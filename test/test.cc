/* vim: set cin ts=4 sw=4 tw=80: */
/*
 * main.cc
 * Copyright (C) 2015 Atri Bhattacharya <badshah400@gmail.com>
 * 
 * cpplotter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpplotter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include "../include/cpplotter.hpp"

using namespace std;
using namespace plotopts;

int main(int argc, char const * argv[])
{
	const unsigned int  NPTS = 11;
	vecd x1, y1, y2;
	for (unsigned int i = 0; i <= NPTS; i += 1)
	{
		x1.push_back(1 + 2 * static_cast<float>(i) / NPTS);
		y1.push_back(sin(x1.back()));
		y2.push_back(log10(x1.back()));
	}
	arrd2 xlims = {x1.front(), x1.back()};
	cpplcanvas can(xlims, {0, 1}, -2);
	cpplplot p(can, argc, argv, PL_PARSE_SKIP);
	p.box("bcghnst", 1.0, 0, "bcgnstv", 0, 0);
	p.setlabels ("x", "f(x)");
	
	linesty ls1(cpplrgbcol (230, 0, 230, 1.0), 1, 1);
	p.linegraph(x1, y1, _nametag = "f#d#ga#u", _plstyle = ls1, _interp = true);
	p.linegraph(x1, y2, _nametag = "f#d#gb#u", _plstyle = ls1);

	fillsty fs1(ls1.Col(), 0, 0);
	fs1.Trans(0.3);
	p.shadec2c(x1, y1, y2, fs1, _nametag = "Shaded reg", _interp = true);

	ptsty   pt1(PTGLYPH["CrossHair"], BLACK, false, 1);
	p.ptgraph(x1, y1, _plstyle = pt1, _thindata = 2);

	arrd2 xline = {0.5, 0.5}, yline = {y1.front(), y1.back()};
	linesty ln1(cpplrgbcol (200, 200, 200, 0.5), 3, 2);
	p.drawline(xline, yline, ln1);

	vecd x2, ypts;
	const unsigned int DOTNPTS = 10;
	for (unsigned int i = 0; i <= DOTNPTS; i++)
	{
		x2.push_back(1 + 2 * static_cast<double>(i)/DOTNPTS);
		ypts.push_back(1.0 / (x2.back()*x2.back()));
	}

	vecd xerrp(x2.size(), 0.1), xerrm(x2.size(), 0.1);
	transform(x2.begin(), x2.end(), xerrp.begin(), xerrp.begin(),
	          std::plus<double>());
	transform(x2.begin(), x2.end(), xerrm.begin(), xerrm.begin(),
	          std::minus<double>());
	
	vecd yerrp(x2.size(), 0.05), yerrm(x2.size(), 0.1);
	transform(ypts.begin(), ypts.end(), yerrp.begin(), yerrp.begin(),
	          std::plus<double>());
	transform(ypts.begin(), ypts.end(), yerrm.begin(), yerrm.begin(),
	          std::minus<double>());

	p.ptgraph(x2, ypts,
	          _plstyle=ptsty(PTGLYPH["FillDiam"],
		                     NAMEDCOL["OliveGreen"], false, 1.5),
	          _xerrdata=boost::make_tuple(xerrm, xerrp),
	          _yerrdata=boost::make_tuple(yerrm, yerrp),
	          _errstyle = errsty(DEF_LINE, true));

	p.ptgraph(x2, ypts,
	          _plstyle=ptsty(PTGLYPH["Diam"],
		                     BLACK, false, 1.5));

	p.draw_legend (_leg_pos = PL_POSITION_LEFT | PL_POSITION_TOP,
	               _leg_style = PL_LEGEND_BOUNDING_BOX | PL_LEGEND_BACKGROUND,
	               _leg_ncols = 1);
	
	return 0;
}

