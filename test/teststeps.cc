/* vim: set cin ts=4 sw=4 tw=80: */
// teststeps.cc
//
// Copyright (C) 2014 - Atri Bhattacharya
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include <functional>
#include "../include/cpplotter.hpp"

using namespace std;
using namespace plotopts;

double ripples(const double & x)
{
	return 0.3 * sin( M_PI * (x - 1) ) + 0.5;
}

int main(int argc, char const * argv[])
{
	const unsigned int  NPTS = 15;
	vecd x1, y1, y2;
	for (unsigned int i = 0; i <= NPTS; i += 1)
	{
		x1.push_back(1 + 2 * static_cast<float>(i) / NPTS);
	}
//	Datatype arrd2 can be used for specifying axes limits.
//	arrd2 xlims = {x1.front(), x1.back()};
	cpplcanvas can({0.8, 3.2}, {-0.2, 1.2}, -2);
	cpplplot p(can, argc, argv, PL_PARSE_SKIP);
	p.box("bcnst", 0, 0, "bcnstv", 0, 0);
	p.setlabels ("x", "y", "Steps in linegraph");

	y1 = x1;
	transform(y1.begin(), y1.end(), y1.begin(), ripples);
	p.linegraph(x1, y1, _nametag="#<monospace/>_steps=\'M\'", _steps='M');
	p.shade2bottom(x1, y1, _steps='m', _fillstyle=fillsty(NAMEDCOL["Green"]));
	p.ptgraph(x1, y1);

	y2.resize(x1.size());
	fill(y2.begin(), y2.end(), -0.2);
	transform(y1.begin(), y1.end(), y2.begin(), y1.begin(), plus<double>());
	p.linegraph(x1, y1, _nametag="#<monospace/>_steps=\'l\'", _steps='l');
	p.ptgraph(x1, y1, _nametag="Exact data points");

//	If _interp=true is specified, _steps has no effect
	transform(y1.begin(), y1.end(), y2.begin(), y1.begin(), plus<double>());
	p.linegraph(x1, y1, _nametag="#<monospace/>_steps=\'r\' _interp=true", _steps='r',
	            _interp=true);
	p.linegraph(x1, y1, _nametag="#<monospace/>_steps=\'r\' _interp=false", _steps='r',
	            _interp=false);


	p.draw_legend(_leg_linesp=2, _leg_style=0);

	return 0;
}

