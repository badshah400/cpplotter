/* vim: set cin ts=4 sw=4 tw=80: */
// testerrs.cc
//
// Copyright (C) 2014 - Atri Bhattacharya
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include <functional>
#include "../include/cpplotter.hpp"

using namespace std;
using namespace plotopts;

// Acknowledgment: http://www.columbia.edu/~fdc/pace/
std::vector<std::string> s = { "Peace",
	                           "Мир",
	                                                              "শান্তি",
	                           "Friede",
	                           "平和",
	                                                                                                                           "שלום",
	                           "Paix",
	                           "Ειρήνη",
                               "Khotso",
                               "Hasîtî",
                               "和平",
                               "Pakjy"};

std::vector<std::string> t = { "سلام",
	                           "Barış",
	                           "Hacaña",
	                                   "평화",
	                           "Rauha",
	                           "Amani",
	                           "สันติภาพ",
                               "Ашти",
                               "Pokój",
                                                                      "عسلامة",
                               "Koosi",
	                           "Filemū"};

int main(int argc, char const * argv[])
{
	cpplcanvas can({0, 1}, {0, 1}, -2);
	cpplplot p(can, argc, argv, PL_PARSE_SKIP);

	float ang = 0;
	for (int i = 0; i < 12; i += 1, ang += 30)
	{
		float radang = ang * M_PI / 180.0;
		p.writetxt(0.45 + 0.1 * cos(radang),
		           0.52 + 0.1 * sin(radang),
		           "#<monospace/>#<bold/>" + t.at(i),
		           _txtrot=ang,
		           _plstyle=txtsty(cpplrgbcol(1.0), 1.1));
	}
	ang = 90;
	for (int i = 0; i < 12; i += 1, ang += 30)
	{
		float radang = ang * M_PI / 180.0;
		p.writetxt(0.45 + 0.27 * cos(radang),
		           0.52 + 0.27 * sin(radang),
		           "#<serif/>" + s.at(i),
		           _txtrot=ang,
		           _plstyle=txtsty(cpplrgbcol(1.0), 2));
	}
	
	return 0;
}

