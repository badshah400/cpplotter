/* vim: set cin ts=4 sw=4 tw=80: */
/*
 * cpplot.hpp
 * Copyright (C) 2015 Atri Bhattacharya <badshah400@gmail.com>
 *
 * cpplotter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpplotter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PLOT_HPP_
#define _PLOT_HPP_

#include <boost/parameter/name.hpp>
#include <boost/parameter/preprocessor.hpp>
#include <boost/tuple/tuple.hpp>
#include <exception>
#include <string>
#include <vector>
#include <algorithm>
#include <interp2d.hpp>
#include <plplot/plstream.h>
#include "styles.hpp"
#include "rgbcol.hpp"
#include "canvas.hpp"

const unsigned int IPOLMULTI = 20;

namespace legendkey
{
	enum {PLTYPE = 0, PLTAG, PLLINESTY, PLPTSTY, PLFILLSTY};
}

static struct legerror : public std::exception
{
	virtual const char* what() const throw()
	{
		return "Legend contruction falied: "
			   "Cannot draw legend with more than 14 colours: "; 
	}
} legenderr;

typedef std::vector<double> vecd;
typedef boost::tuple<vecd, vecd> minmaxerr;
typedef boost::tuple<int, std::string, linesty, ptsty, fillsty> legtuple;
typedef std::vector<legtuple> vecleg;

typedef array<bool, 2>    bool2;
typedef array<bool, 4>    bool4;
typedef array<linesty, 4> lsty4;

const linesty DEFMAJGRIDLINE(cpplrgbcol (100, 100, 100, 1.0), 4, 0.6);
const linesty DEFMINGRIDLINE(cpplrgbcol (100, 100, 100, 1.0), 2, 0.3);
const bool4   DEFGRIDS   = {false, false, false, false};
const lsty4   DEFGRIDSTY = {DEFMAJGRIDLINE, DEFMINGRIDLINE,
	                        DEFMAJGRIDLINE, DEFMINGRIDLINE};
static vecleg LEGSTYLE;

namespace param = boost::parameter;

namespace plotopts
{
	BOOST_PARAMETER_NAME(xpos)
	BOOST_PARAMETER_NAME(ypos)
	BOOST_PARAMETER_NAME(textstr)
	BOOST_PARAMETER_NAME(xdata)
	BOOST_PARAMETER_NAME(ydata)
	BOOST_PARAMETER_NAME(plstyle)
	BOOST_PARAMETER_NAME(txtrot)
	BOOST_PARAMETER_NAME(thindata)
	BOOST_PARAMETER_NAME(errstyle)
	BOOST_PARAMETER_NAME(nametag)
	BOOST_PARAMETER_NAME(steps)
	BOOST_PARAMETER_NAME(x1data)
	BOOST_PARAMETER_NAME(x2data)
	BOOST_PARAMETER_NAME(y1data)
	BOOST_PARAMETER_NAME(y2data)
	BOOST_PARAMETER_NAME(xerrdata)
	BOOST_PARAMETER_NAME(yerrdata)
	BOOST_PARAMETER_NAME(l1style)
	BOOST_PARAMETER_NAME(l2style)
	BOOST_PARAMETER_NAME(interp)
	BOOST_PARAMETER_NAME(fillstyle)
	BOOST_PARAMETER_NAME(leg_pos)
	BOOST_PARAMETER_NAME(leg_style)
	BOOST_PARAMETER_NAME(leg_bgcol)
	BOOST_PARAMETER_NAME(leg_bboxcol)
	BOOST_PARAMETER_NAME(leg_ncols)
	BOOST_PARAMETER_NAME(leg_fontsc)
	BOOST_PARAMETER_NAME(leg_linesp)
	BOOST_PARAMETER_NAME(leg_plwid)
}

template <class T>
T takelog(const T & v, const double & thres = -50)
{
	T res(v);
	for (unsigned int i = 0; i < res.size(); ++i)
	{
		res[i] = (v[i] > 0 ? log10(v[i]) : thres-1);
	}

	return res;
}

template <>
double takelog<double>(const double & v, const double & thres)
{
	return (v > 0 ? log10(v) : thres);
}

template <>
float takelog<float>(const float & v, const double & thres)
{
	return (v > 0 ? log10(v) : thres);
}

class cpplplot: public plstream 
{
private:
	double xmajtick;
	int    xmintick;
	double ymajtick;
	int    ymintick;
	bool2  logaxes;
	void draw_xerrs(const unsigned int n,
                    const vecd & xarr, const vecd & yarr,
                    const vecd & xerrm, const vecd xerrp,
                    errsty errstyle);
	
	void draw_yerrs(const unsigned int n,
	                const vecd & xarr, const vecd & yarr,
	                const vecd & yerrm, const vecd & yerrp,
	                errsty errstyle);

	void step_data(vecd & xdata, vecd & ydata, const char c);

	inline void grids(const bool4 & _g = {true, true, true, true},
	                  const lsty4 & _s = {DEFMAJGRIDLINE, DEFMINGRIDLINE,
					                      DEFMAJGRIDLINE, DEFMINGRIDLINE})
	{
		using std::string;
		string xstr, ystr;
		string xlog = (logaxes[0] == true ? "l" : "");
		string ylog = (logaxes[1] == true ? "l" : "");
		for (unsigned int i = 0; i < 4; ++i)
		{
			if (_g[i])
			{
				cpplrgbcol col = _s[i].Col();
				this->scol0a(14,
						     col.get<RED>(),
						     col.get<GREEN>(),
						     col.get<BLUE>(),
						     col.get<TRANS>());
				this->col0(14);
				this->width(_s[i].Thickness());
				this->lsty(_s[i].Style());

				std::string spec(i % 2 ? "gh" : "g");
				spec += (i < 2 ? xlog : ylog);
				this->plstream::box(i < 2 ? spec.data() : "",
				                    xmajtick, xmintick,
				                    i > 1 ? spec.data() : "",
				                    ymajtick, ymintick);
			}
			
		}
		
		this->width(0);
		this->lsty(1);
		this->col0(1);
	}
	
public:
    inline cpplplot(int argc = 1, const char ** argv = nullptr,
                    const int parse_mode = PL_PARSE_FULL)
    {
		this->scolbga(255,255,255,0);
		this->scol0(1,0,0,0);
		this->scol0(15,255,0,0);
        if (argc > 1 && argv != nullptr)
		    this->parseopts(&argc, argv, parse_mode);
		this->init();        
    }

	inline cpplplot(const cpplcanvas & c)
	{
		this->scolbga(255,255,255,0);
		this->scol0(1,0,0,0);
		this->scol0(15,255,0,0);
		this->init();
		this->env(c.x_lims()[0], c.x_lims()[1],
		          c.y_lims()[0], c.y_lims()[1],
		          0, c.axesflag());

		if (c.axesflag() < 10)
			logaxes = {false, false};
		else if (c.axesflag() < 20)
			logaxes = {true, false};
		else if (c.axesflag() < 30)
			logaxes = {false, true};
		else
			logaxes = {true, true};
	}

	inline cpplplot(const cpplcanvas & c, int & argc, const char ** argv,
	                const int parse_mode = PL_PARSE_FULL)
	{
		this->scolbg(255,255,255);
		this->scol0(1,0,0,0);
		this->scol0(15,255,0,0);
		this->parseopts(&argc, argv, parse_mode);
		this->init();
		this->env(c.x_lims()[0], c.x_lims()[1],
		          c.y_lims()[0], c.y_lims()[1],
		          0, c.axesflag());

		if (c.axesflag() < 10)
			logaxes = {false, false};
		else if (c.axesflag() < 20)
			logaxes = {true, false};
		else if (c.axesflag() < 30)
			logaxes = {false, true};
		else
			logaxes = {true, true};
	}

	inline cpplplot(const cpplcanvas & c, int & argc, const char ** argv,
	                const std::string & devname, const std::string & fname)
	{
		this->scolbg(255,255,255);
		this->scol0(1,0,0,0);
		this->scol0(15,255,0,0);
		this->parseopts(&argc, argv, PL_PARSE_FULL);
		this->sdev(devname.c_str());
		this->sfnam(fname.c_str());
		this->init();
		this->env(c.x_lims()[0], c.x_lims()[1],
		          c.y_lims()[0], c.y_lims()[1],
		          0, c.axesflag());

		if (c.axesflag() < 10)
			logaxes = {false, false};
		else if (c.axesflag() < 20)
			logaxes = {true, false};
		else if (c.axesflag() < 30)
			logaxes = {false, true};
		else
			logaxes = {true, true};

	}

	inline void setlabels(const std::string & xlabel = "",
	                      const std::string & ylabel = "",
	                      const std::string & title  = "")
	{
		this->lab(xlabel.c_str(), ylabel.c_str(), title.c_str());
	}

	inline bool2 is_logaxes() { return logaxes; }

	inline void box(const std::string & _xaxstr, const double & _xmajtk,
	                const int & _xmintk,
	                const std::string & _yaxstr, const double & _ymajtk,
	                const int & _ymintk,
	                const lsty4 & _s = {DEFMAJGRIDLINE, DEFMINGRIDLINE,
					                    DEFMAJGRIDLINE, DEFMINGRIDLINE})
	{
		using std::string;
		xmajtick = _xmajtk;
		xmintick = _xmintk;
		ymajtick = _ymajtk;
		ymintick = _ymintk;
		string xstr = _xaxstr, ystr = _yaxstr;
		bool4  grid_state = DEFGRIDS;

//		Determine if logscale is requested for any of the axes: 0 -> x-axis, 1 -> y-axis
		logaxes[0] = (xstr.find('l') == string::npos ? false : true);
		logaxes[1] = (ystr.find('l') == string::npos ? false : true);

		for (unsigned int i = 0; i < 4; ++i)
		{
			string::size_type index;
			string & axstr = (i > 1 ? xstr : ystr);
			char ch = (i % 2 ? 'h' : 'g');

			index = axstr.find(ch);
			if (index != string::npos)
			{
				grid_state[i] = true;
				axstr.erase(index, 1);
			}
		}

		if (grid_state != DEFGRIDS)
		{
			this->grids(grid_state, _s);	
		}
		
		this->plstream::box(xstr.c_str(), xmajtick, xmintick,
		                    ystr.c_str(), ymajtick, ymintick);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION (
	                                 (void),
	                                 writetxt,
	                                 plotopts::tag,
	                                 (required (xpos, (const float))
	                                           (ypos, (const float))
	                                           (textstr, (const std::string)))
	                                 (optional (plstyle, (const txtsty &),
	                                            DEF_TXT)
	                                           (txtrot, (const float &), 0))
	                                 )
	{
		float angrad = txtrot * M_PI / 180.0;
		float dx = cos(angrad), dy = sin(angrad);
		cpplrgbcol col = plstyle.Col();
		float s = plstyle.Size();
		this->scol0a(14,
		             col.get<RED>(),
		             col.get<GREEN>(),
		             col.get<BLUE>(),
		             col.get<TRANS>());
		this->col0(14);
		this->schr(0, s);

/*		Get min x and y axes limits for taking eff. log of -ve data, in case*/
		double lxmin, lxmax, lymin, lymax;
		this->gvpw(lxmin, lxmax, lymin, lymax);
		
		this->ptex(this->is_logaxes()[0] ? takelog(xpos, lxmin) : xpos,
		           this->is_logaxes()[1] ? takelog(ypos, lymin) : ypos,
		           dx,
		           dy,
		           plstyle.Just(), textstr.c_str());
		this->schr(0, 1);
		this->width(0);
		this->col0(1);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                drawline,
	                                plotopts::tag,
	                                (required (xdata, (arrd2) )
	                                          (ydata, (arrd2) ))
	                                (optional (plstyle, (const linesty &),
	                                           DEF_LINE) )
	                               )
	{
		cpplrgbcol col = plstyle.Col();
		this->scol0a(14,
		             col.get<RED>(),
		             col.get<GREEN>(),
		             col.get<BLUE>(),
		             col.get<TRANS>());
		this->col0(14);
		this->width(plstyle.Thickness());
		this->lsty(plstyle.Style());

/*		Get min x and y axes limits for taking eff. log of -ve data, in case*/
		double lxmin, lxmax, lymin, lymax;
		this->gvpw(lxmin, lxmax, lymin, lymax);
		
		this->line(2,
		           this->is_logaxes()[0] ? takelog(xdata, lxmin).data() : xdata.data(),
		           this->is_logaxes()[1] ? takelog(ydata, lymin).data() : ydata.data());
		this->width(0);
		this->lsty(1);
		this->col0(1);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                linegraph,
	                                plotopts::tag,
	                                (required (xdata,   (vecd) )
	                                          (ydata,   (vecd) ) )
	                                (optional (plstyle, (const linesty &),
	                                           linesty())
	                                          (interp,  (const bool), false)
	                                          (nametag, (std::string), "")
	                                          (steps,   (const char),  '0'))
	                                )
	{
		vecd xnew, ynew;
		vecd::iterator itx, ity;
		if (!interp)
		{		
			step_data(xdata, ydata, steps);
		}

		cpplrgbcol col = plstyle.Col();
		this->scol0a(14,
		             col.get<RED>(),
		             col.get<GREEN>(),
		             col.get<BLUE>(),
		             col.get<TRANS>());
		this->col0(14);

		this->lsty(plstyle.Style());
		this->width(plstyle.Thickness());

/*		Get min x and y axes limits for taking eff. log of -ve data, in case*/
		double lxmin, lxmax, lymin, lymax;
		this->gvpw(lxmin, lxmax, lymin, lymax);
		
		if (logaxes[0])
			xdata = takelog(xdata, lxmin);

		if (logaxes[1])
			ydata = takelog(ydata, lymin);

		unsigned int n = xdata.size();				
		vecd xarr, yarr;

		if (interp)
		{
			interp_data id(n, xdata.begin(), ydata.begin());
			n *= IPOLMULTI;
			for (unsigned int i = 0; i < n; ++i)
			{
				xarr.push_back(xdata.front()
					           + i * (xdata.back() - xdata.front()) / (n - 1));
				yarr.push_back(id.interp_akima(xarr[i]));
			}			
		}
		else
		{
			xarr = xdata;
			yarr = ydata;
		}
		
		this->line(n, xarr.data(), yarr.data());

		this->col0(1);
		this->lsty(1);
		this->width(0);

		if (!nametag.empty())
		{
			legtuple lt = boost::make_tuple(PL_LEGEND_LINE,
			                                nametag,
			                                plstyle,
			                                DEF_PT,
			                                DEF_AREAFILL);
			LEGSTYLE.push_back(lt);
		}
		
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                ptgraph,
	                                plotopts::tag,
	                                (required (xdata,    (vecd) )
	                                          (ydata,    (vecd) ) )
	                                (optional (plstyle,  (const ptsty &),
	                                           ptsty())
	                                          (nametag,  (std::string), "")
	                                          (thindata, (int), 1)
	                                          (xerrdata,   (minmaxerr),
	                                           minmaxerr())
	                                          (yerrdata,   (minmaxerr),
	                                           minmaxerr())
	                                          (errstyle, (const errsty &),
	                                           errsty()))
	                                )
	{
		const unsigned int n = xdata.size() / thindata;
		vecd xarr, yarr, xerrmax, xerrmin, yerrmax, yerrmin;
		vecd x1data = boost::get<0>(xerrdata),
		     x2data = boost::get<1>(xerrdata),
		     y1data = boost::get<0>(yerrdata),
		     y2data = boost::get<1>(yerrdata);

		for (unsigned int i = 0; i < n; ++i)
		{
			const unsigned int thinidx = thindata * i;
			xarr.push_back(xdata.at(thinidx));
			yarr.push_back(ydata.at(thinidx));

			if (x1data.empty())
				xerrmin.push_back(0);
			else
				xerrmin.push_back(x1data.at(thinidx));
			if (x2data.empty())
				xerrmax.push_back(0);
			else
				xerrmax.push_back(x2data.at(thinidx));

			if (y1data.empty())
				yerrmin.push_back(0);
			else
				yerrmin.push_back(y1data.at(thinidx));
			if (y2data.empty())
				yerrmax.push_back(0);
			else
				yerrmax.push_back(y2data.at(thinidx));
		}
		
		if (!x1data.empty() || !x2data.empty())
			this->draw_xerrs(n, xarr, yarr, xerrmin, xerrmax, errstyle);
		
		if (!y1data.empty() || !y2data.empty())
			this->draw_yerrs(n, xarr, yarr, yerrmin, yerrmax, errstyle);
		
		cpplrgbcol col = plstyle.Col();
		this->scol0a(14,
		             col.get<RED>(),
		             col.get<GREEN>(),
		             col.get<BLUE>(),
		             col.get<TRANS>());
		this->col0(14);	
		this->sfont(-1, -1, -1);
		this->schr(0, plstyle.Size());
		this->width(plstyle.Thickness());

/*		Get min x and y axes limits for taking eff. log of -ve data, in case*/
		double lxmin, lxmax, lymin, lymax;
		this->gvpw(lxmin, lxmax, lymin, lymax);
		
		this->string(n,
		             logaxes[0] ? takelog(xarr, lxmin).data() : xarr.data(),
		             logaxes[1] ? takelog(yarr, lymin).data() : yarr.data(),
		             plstyle.Glyph().c_str());
		this->sfont(-1, -1, PL_FCI_MEDIUM);
		this->width(0);
		this->col0(1);
		this->schr(0, 1);

		if (!nametag.empty())
		{
			legtuple lt = boost::make_tuple(PL_LEGEND_SYMBOL,
			                                nametag,
			                                DEF_LINE,
			                                plstyle,
			                                DEF_AREAFILL);
			LEGSTYLE.push_back(lt);
		}
	}


	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                shadec2c,
	                                plotopts::tag,
	                                (required (x1data,   (vecd) )
	                                          (y1data,   (vecd) )
	                                          (y2data,   (vecd) ))
	                                (optional (fillstyle, (const fillsty &),
	                                           fillsty())
	                                          (interp,   (const bool), false)
	                                          (steps, (const char), '0')
	                                          (nametag, (std::string), ""))
	                                )
	{
		using namespace plotopts;

		if(!interp)
		{
			vecd xdat = x1data;
			step_data (x1data, y1data, steps);
			x1data    = xdat;
			step_data (x1data, y2data, steps);
		}
		unsigned int n = 2 * x1data.size();
		cpplrgbcol col1 = fillstyle.Col();

/*		Get min x and y axes limits for taking eff. log of -ve data, in case*/
		double lxmin, lxmax, lymin, lymax;
		this->gvpw(lxmin, lxmax, lymin, lymax);
		
		if (logaxes[0])
			x1data = takelog(x1data, lxmin);

		if (logaxes[1])
		{
			y1data = takelog(y1data, lymin);
			y2data = takelog(y2data, lymin);
		}

		vecd xx, yy;
		if (interp)
		{
			n *= IPOLMULTI;
			interp_data id1(x1data.size(), x1data.begin(), y1data.begin());
			interp_data id2(x1data.size(), x1data.begin(), y2data.begin());

			vecd yy2;
			for (unsigned int i = 0; i < n / 2; ++i)
			{
				double ix
					= x1data.front()
					  + i * 2 * (x1data.back() - x1data.front()) / (n - 1);
				xx.push_back(ix);
				yy.push_back(id1.interp_akima(xx.back()));
				yy2.push_back(id2.interp_akima(xx.back()));
			}
			vecd xxtmp = xx, yytmp = yy;
			std::reverse(xx.begin(), xx.end());
			std::reverse(yy2.begin(), yy2.end());
			xxtmp.insert(xxtmp.end(), xx.begin(), xx.end());
			yytmp.insert(yytmp.end(), yy2.begin(), yy2.end());
			xx = xxtmp;
			yy = yytmp;
		}
		else
		{
			xx = x1data; yy = y1data;
			std::reverse(x1data.begin(), x1data.end());
			std::reverse(y2data.begin(), y2data.end());
			xx.insert(xx.end(), x1data.begin(), x1data.end());
			yy.insert(yy.end(), y2data.begin(), y2data.end());
		}
		
		this->psty(fillstyle.Style());
		this->width(fillstyle.Thickness());
		this->scol0a(14,
		             col1.get<RED>(),
		             col1.get<GREEN>(),
		             col1.get<BLUE>(),
		             col1.get<TRANS>());
		this->col0(14);

		this->fill(n, xx.data(), yy.data());
		
		this->psty(0);
		this->col0(1);
		this->width(0);

		if (!nametag.empty())
		{
			legtuple lt = boost::make_tuple(PL_LEGEND_COLOR_BOX,
			                                nametag,
			                                DEF_LINE,
			                                DEF_PT,
			                                fillstyle);
			LEGSTYLE.push_back(lt);
		}

	}


	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                shade2bottom,
	                                plotopts::tag,
	                                (required (xdata,   (vecd) )
	                                          (ydata,   (vecd) ))
	                                (optional (fillstyle, (const fillsty &),
	                                           fillsty())
	                                          (interp,   (const bool), false)
	                                          (steps, (const char), '0')
	                                          (nametag, (std::string), ""))
	                                )
	{
		using namespace plotopts;
		double tmp, plot_ymin;
		this->gvpw(tmp, tmp, plot_ymin, tmp);
		vecd y2data;
		y2data.assign(xdata.size(), plot_ymin);
		
		this->shadec2c(xdata, ydata, y2data, fillstyle, interp, steps, nametag);
	}

	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                shade2top,
	                                plotopts::tag,
	                                (required (xdata,   (vecd) )
	                                          (ydata,   (vecd) ))
	                                (optional (fillstyle, (const fillsty &),
	                                           DEF_AREAFILL)
	                                          (interp,   (const bool), false)
	                                          (steps, (const char), '0')
	                                          (nametag, (std::string), "") )
	                                )
	{
		using namespace plotopts;
		double tmp, plot_ymax;
		this->gvpw(tmp, tmp, tmp, plot_ymax);
		vecd y2data;
		y2data.assign(xdata.size(), plot_ymax);

		this->shadec2c(xdata, ydata, y2data, fillstyle, interp, steps, nametag);
	}
	
	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                shade2x,
	                                plotopts::tag,
	                                (required (xdata,   (vecd) )
	                                          (ydata,   (vecd) ))
	                                (optional (fillstyle, (const fillsty &),
	                                           fillsty())
	                                          (interp,   (const bool), false)
	                                          (steps, (const char), '0')
	                                          (nametag, (std::string), "") )
	                                )
	{
		using namespace plotopts;
		double tmp;
		vecd y2data;
		y2data.assign(xdata.size(), 0);
		
		this->shadec2c(xdata, ydata, y2data, fillstyle, interp, steps, nametag);
	}


	BOOST_PARAMETER_MEMBER_FUNCTION(
	                                (void),
	                                draw_legend,
	                                plotopts::tag,
	                                (optional (leg_pos,   (const int),
	                                           PL_POSITION_RIGHT | PL_POSITION_TOP)
	                                          (leg_style, (const int),
	                                           PL_LEGEND_BACKGROUND | PL_LEGEND_BOUNDING_BOX)
	                                          (leg_bgcol, (const cpplrgbcol),
	                                           cpplrgbcol (230, 230, 230, 0.9))
	                                          (leg_bboxcol, (const cpplrgbcol),
	                                           cpplrgbcol (160, 160, 160, 1.0))
	                                          (leg_ncols, (unsigned int), 1)
	                                          (leg_fontsc, (const float), 0.9)
	                                          (leg_linesp, (const float), 2.7)
	                                          (leg_plwid, (const float), 0.1))
	                                )
	{
//		draw_legend will need to use a modified colour map;
//		store the default map first and restore it at the end.
		unsigned int   defmaxcolidx = 16;      // Default col0 map has 16 colours
		vector<int>    defR(defmaxcolidx),     // Default Reds
		               defG(defmaxcolidx),     // Default Greens
		               defB(defmaxcolidx);     // Default Blues
		vector<double> defA(defmaxcolidx);     // Default Alphas
		for(unsigned int i = 0; i < defmaxcolidx; ++i)
		{
			this->gcol0a(i, defR[i], defG[i], defB[i], defA[i]);
		}
		
		using namespace legendkey;
		unsigned int nleg = LEGSTYLE.size();
/*		if (nleg > 10)
			throw legenderr;
*/
		vector<int>          legopts, textcols;

		vector<int>          linetypes, linecols;
		vecd                 linewidths;

		vector<int>          filltypes, fillcols;
		vecd                 fillwidths, fillscales;

		vector<int>          symnos, symcols;
		vecd                 symscales;
		vector<const char *> symchars; 

		vector<cpplrgbcol>   cols;
		vector<const char *> tags;;

		int ncols = nleg + 4;
		vector<int> rcols(ncols), gcols(ncols), bcols(ncols);
		vecd trnps(ncols);

		cpplrgbcol legbg = leg_bgcol, legbbox = leg_bboxcol;
		this->scol0a(2, legbg.get<RED>(),
		                legbg.get<GREEN>(),
		                legbg.get<BLUE>(),
		                legbg.get<TRANS>());
		this->scol0a(3, legbbox.get<RED>(),
		                legbbox.get<GREEN>(),
		                legbbox.get<BLUE>(),
		                legbbox.get<TRANS>());
		
		for (unsigned int i = 0; i < 4; ++i)
		{
			this->gcol0a(i, rcols[i], gcols[i], bcols[i], trnps[i]);
		}
		
		for (unsigned int i = 0; i < nleg; ++i)
		{
			legtuple lsi      = LEGSTYLE.at(i);
			legopts.push_back(lsi.get<PLTYPE>());
			textcols.push_back(1);
			tags.push_back(lsi.get<PLTAG>().c_str());
			switch (legopts.back())
			{
				case PL_LEGEND_LINE:
					cols.push_back(lsi.get<PLLINESTY>().Col());
					break;
				case PL_LEGEND_SYMBOL:
					cols.push_back(lsi.get<PLPTSTY>().Col());
					break;
				case PL_LEGEND_COLOR_BOX:
					cols.push_back(lsi.get<PLFILLSTY>().Col());
					break;
			}
			cpplrgbcol icol   = cols.back();
			rcols[i+4]        = icol.get<RED>();
			gcols[i+4]        = icol.get<GREEN>();
			bcols[i+4]        = icol.get<BLUE>();
			trnps[i+4]        = icol.get<TRANS>();

			linetypes.push_back(lsi.get<PLLINESTY>().Style());
			linewidths.push_back(lsi.get<PLLINESTY>().Thickness());

			filltypes.push_back(lsi.get<PLFILLSTY>().Style());
			fillwidths.push_back(lsi.get<PLFILLSTY>().Thickness());
			fillscales.push_back(0.75);

			symnos.push_back(1);
			symchars.push_back(lsi.get<PLPTSTY>().Glyph().c_str());
			symscales.push_back(lsi.get<PLPTSTY>().Size());
		}

		this->scmap0a(rcols.data(), gcols.data(), bcols.data(),
		              trnps.data(), ncols);
		for (unsigned int i = 0; i < nleg; ++i)
		{
			linecols.push_back(i+4);
			fillcols.push_back(i+4);
			symcols.push_back(i+4);
		}

		double legw, legh;
		leg_ncols = ((leg_ncols > nleg || leg_ncols <= 0) ? 1 : leg_ncols); 

		this->legend(
		             &legw, &legh,           // Stores leg width and height
		             leg_style,              // Bits controlling overall leg appearance
		             leg_pos,                // Leg position
		             0.03, 0.03,             // x,y offset from leg position
		             leg_plwid,              // Horizontal width of plot-area
		             2, 3, 1,                // Bg colour, leg box colour and line style
		             (nleg + (nleg%leg_ncols ? 1 : 0)) / leg_ncols, leg_ncols,
		                                     // # of rows, cols
		             nleg,                   // # of total leg entries
		             legopts.data(),         // Array of leg plots
		             0.75, leg_fontsc, leg_linesp, 0,
		                                     // Text offset, scale, line spacing and justification
		             textcols.data(),        // Array of text cols
		             tags.data(),            // Array of string annotations
		             fillcols.data(),   filltypes.data(),
		             fillscales.data(), fillwidths.data(),
		                                     // Array of fill specs for solidbox plots
		             linecols.data(),   linetypes.data(),  linewidths.data(),
		                                     // Array of line specs for line plots
		             symcols.data(),    symscales.data(),  symnos.data(),
		             symchars.data()
		                                     // Array of pt specs for pt plots
		            );
		
//		Restore default colour map at the end.
		this->scmap0a(defR.data(), defG.data(), defB.data(),
		              defA.data(), defmaxcolidx);
	}
};

void cpplplot::draw_xerrs(const unsigned int n,
                          const vecd & xarr, const vecd & yarr,
                          const vecd & xerrm, const vecd xerrp,
                          errsty errstyle)
{
	double xwmin, xwmax, ywmin, ywmax;
	this->gvpw(xwmin, xwmax, ywmin, ywmax);
	double caplen = 0.01 * (ywmax - ywmin);

	for (unsigned int i = 0; i < n; i++)
	{
		arrd2 ypt = {yarr[i], yarr[i]}, xpt;
		if (xerrm[i])
		{
			xpt = {xarr[i], xerrm[i]};
			drawline(xpt, ypt, plotopts::_plstyle=errstyle.LineStyle());
			if (errstyle.DrawCaps())
			{
			//  Draw endcaps for errorbars
				drawline(arrd2{xerrm[i], xerrm[i]},
				         arrd2{ypt[1]-caplen, ypt[1]+caplen},
				         plotopts::_plstyle=errstyle.LineStyle());
			}					
		}
		if (xerrp[i])
		{
			xpt = {xarr[i], xerrp[i]};
			drawline(xpt, ypt, plotopts::_plstyle=errstyle.LineStyle());
			if (errstyle.DrawCaps())
			{
			//  Draw endcaps for errorbars
				drawline(arrd2{xerrp[i], xerrp[i]},
				         arrd2{ypt[1]-caplen, ypt[1]+caplen},
				         plotopts::_plstyle=errstyle.LineStyle());
			}					
		}
	}
}

void cpplplot::draw_yerrs(const unsigned int n,
                          const vecd & xarr, const vecd & yarr,
                          const vecd & yerrm, const vecd & yerrp,
                          errsty errstyle)
{
	double xwmin, xwmax, ywmin, ywmax;
	this->gvpw(xwmin, xwmax, ywmin, ywmax);
	double caplen = 0.01 * (xwmax - xwmin);

	for (unsigned int i = 0; i < n; i++)
	{
		arrd2 xpt = {xarr[i], xarr[i]}, ypt;
		if (yerrm[i])
		{
			ypt = {yarr[i], yerrm[i]};
			drawline(xpt, ypt, plotopts::_plstyle=errstyle.LineStyle());
			if (errstyle.DrawCaps())
			{
			//  Draw endcaps for errorbars
				drawline(arrd2{xarr[i]-caplen, xarr[i]+caplen},
				         arrd2{ypt[1], ypt[1]},
				         plotopts::_plstyle=errstyle.LineStyle());
			}					
		}
		if (yerrp[i])
		{
			ypt = {yarr[i], yerrp[i]};
			drawline(xpt, ypt, plotopts::_plstyle=errstyle.LineStyle());
			if (errstyle.DrawCaps())
			{
			//  Draw endcaps for errorbars
				drawline(arrd2{xarr[i]-caplen, xarr[i]+caplen},
				         arrd2{ypt[1], ypt[1]},
				         plotopts::_plstyle=errstyle.LineStyle());
			}					
		}
	}
}

void cpplplot::step_data(vecd & xdata, vecd & ydata, const char c)
{
	vecd xnew, ynew;
	double tmp, plot_ymin;
	this->gvpw(tmp, tmp, plot_ymin, tmp);

	switch (c)
	{
		case 'l':
			xnew.push_back(xdata.front());
			for (unsigned int i = 1; i < xdata.size()-1; i++)
			{
				xnew.push_back(xdata.at(i));
				xnew.push_back(xdata.at(i));
			}
			xnew.push_back(xdata.back());
			for (unsigned int i = 0; i < ydata.size()-1; i++)
			{
				ynew.push_back(ydata.at(i));
				ynew.push_back(ydata.at(i));
			}
			// Data points to ensure the steps start and end at viewport bottom
			xnew.insert(xnew.begin(), xdata.front());
			ynew.insert(ynew.begin(), plot_ymin);

			xnew.insert(xnew.end(), xdata.back());
			ynew.insert(ynew.end(), plot_ymin);
			break;

		case 'L':
			xnew.push_back(xdata.front());
			for (unsigned int i = 1; i < xdata.size()-1; i++)
			{
				xnew.push_back(xdata.at(i));
				xnew.push_back(xdata.at(i));
			}
			xnew.push_back(xdata.back());
			for (unsigned int i = 0; i < ydata.size()-1; i++)
			{
				ynew.push_back(ydata.at(i));
				ynew.push_back(ydata.at(i));
			}
			break;

		case 'r':
			for (unsigned int i = 0; i < xdata.size()-1; i++)
			{
				xnew.push_back(xdata.at(i));
				xnew.push_back(xdata.at(i));
			}
			xnew.push_back(xdata.back());

			ynew.push_back(ydata.front());
			for (unsigned int i = 1; i < ydata.size(); i++)
			{
				ynew.push_back(ydata.at(i));
				ynew.push_back(ydata.at(i));
			}
			// Data points to ensure the steps start and end at viewport bottom
			xnew.insert(xnew.begin(), xdata.front());
			ynew.insert(ynew.begin(), plot_ymin);

			xnew.insert(xnew.end(), xdata.back());
			ynew.insert(ynew.end(), plot_ymin);

			break;

		case 'R':
			for (unsigned int i = 0; i < xdata.size()-1; i++)
			{
				xnew.push_back(xdata.at(i));
				xnew.push_back(xdata.at(i));
			}
			xnew.push_back(xdata.back());

			ynew.push_back(ydata.front());
			for (unsigned int i = 1; i < ydata.size(); i++)
			{
				ynew.push_back(ydata.at(i));
				ynew.push_back(ydata.at(i));
			}

			break;

		case 'm':
			xnew.push_back(xdata.front());
			for (unsigned int i = 1; i < xdata.size(); i++)
			{
				double xmean = 0.5 * (xdata.at(i-1) + xdata.at(i));
				xnew.push_back(xmean);
				xnew.push_back(xmean);
			}
			xnew.push_back(xdata.back());

			for (unsigned int i = 0; i < ydata.size(); i++)
			{
				ynew.push_back(ydata.at(i));
				ynew.push_back(ydata.at(i));
			}
			// Data points to ensure the steps start and end at viewport bottom
			xnew.insert(xnew.begin(), xdata.front());
			ynew.insert(ynew.begin(), plot_ymin);

			xnew.insert(xnew.end(), xdata.back());
			ynew.insert(ynew.end(), plot_ymin);
			
			break;

		case 'M':
			xnew.push_back(xdata.front());
			for (unsigned int i = 1; i < xdata.size(); i++)
			{
				double xmean = 0.5 * (xdata.at(i-1) + xdata.at(i));
				xnew.push_back(xmean);
				xnew.push_back(xmean);
			}
			xnew.push_back(xdata.back());

			for (unsigned int i = 0; i < ydata.size(); i++)
			{
				ynew.push_back(ydata.at(i));
				ynew.push_back(ydata.at(i));
			}
			
			break;

		default:
			xnew = xdata; ynew = ydata;
			break;
	}
	xdata = xnew; ydata = ynew;
}

#endif // _CPPLOT_HPP_

