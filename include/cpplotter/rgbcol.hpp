/* vim: set cin ts=4 sw=4 tw=80: */
/*
 * rgbcol.hpp
 *
 * Copyright (C) 2014 - Atri Bhattacharya
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RGBCOL_HPP_
#define RGBCOL_HPP_

#include <boost/tuple/tuple.hpp>
#include <string>
#include <map>
#include <exception>

enum {RED = 0, GREEN, BLUE, TRANS};

typedef boost::tuple<unsigned int, unsigned int, unsigned int, float> cpplrgb;

inline bool validrgb(const unsigned int & d)
{
	return (d <= 255? true : false);
}

inline bool validtrans(const float & f)
{
	return ((f >= 0 && f <= 1)? true: false);
}

static struct rgbdomainerr : public std::exception
{
	virtual const char* what() const throw()
	{
		return "rgbcol object construction failed: " 
	           "Values should be between 0 and 255.\n";
	}
} rgberr;

static struct transdomainerr : public std::exception
{
	virtual const char* what() const throw()
	{
		return "rgbcol object construction failed: " 
	           "Transparency should be between 0 and 1.\n";
	}
} transerr;


class cpplrgbcol : public cpplrgb
{
private:
	static std::map<std::string, const cpplrgbcol>::const_iterator _colit;
public:
	cpplrgbcol() : cpplrgb(0, 0, 0, 1)
	{  }
	cpplrgbcol(const float &);
	cpplrgbcol(const unsigned int &r,
	           const unsigned int &g,
	           const unsigned int &b,
	           const float & tr)
		: cpplrgb(r, g, b, tr)
	{
		if (! (validrgb(r) && validrgb(g) && validrgb(b)) )
			throw rgberr;
		if (! validtrans(tr) )
			throw transerr;
	}
	cpplrgbcol(const cpplrgbcol & rcol)
		: cpplrgb(rcol.get<RED>(),
		          rcol.get<GREEN>(),
		          rcol.get<BLUE>(),
		          rcol.get<TRANS>())
	{ };
};

typedef std::map<std::string, const cpplrgbcol>    cpplrgbmap;

cpplrgbmap NAMEDCOL(
              {{"Red"         , cpplrgbcol (225,   0,   0, 1)},
               {"Green"       , cpplrgbcol (  0, 200,   0, 1)},
               {"Blue"        , cpplrgbcol (  0,   0, 220, 1)},
               {"Magenta"     , cpplrgbcol (225,   0, 225, 1)}, 
               {"Yellow"      , cpplrgbcol (225, 225,   0, 1)},
               {"Brown"       , cpplrgbcol (162,  42,  42, 1)},
               {"OliveGreen"  , cpplrgbcol ( 74, 138, 110, 1)},
               {"FireBrick"   , cpplrgbcol (255, 127,   0, 1)},
               {"SlateBlue"   , cpplrgbcol ( 72,  61, 139, 1)},
               {"DeepPink"    , cpplrgbcol (225,  20, 137, 1)}
              });


static const cpplrgbcol BLACK (0, 0, 0, 1);

cpplrgbmap::const_iterator cpplrgbcol::_colit = NAMEDCOL.begin();

cpplrgbcol::cpplrgbcol(const float & tr)
{
	if(_colit == NAMEDCOL.end())
	   _colit = NAMEDCOL.begin();
	float r, g, b;
	r = (_colit->second).get<RED>();
	g = (_colit->second).get<GREEN>();
	b = (_colit->second).get<BLUE>();
	if (! (validrgb(r) && validrgb(g) && validrgb(b)) )
		throw rgberr;
	if (! validtrans(tr) )
		throw transerr;
	this->get<RED>()   = r;
	this->get<GREEN>() = g;
	this->get<BLUE>()  = b;
	this->get<TRANS>() = tr;
	++_colit;
}


#endif /*LOCBGR_PPH_ */
