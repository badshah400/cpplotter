/* vim: set cin ts=4 sw=4 tw=80: */
/*
 * styles.hpp
 *
 * Copyright (C) 2014 - Atri Bhattacharya
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STYLES_HPP_
#define STYLES_HPP_

#include <string>
#include <map>
#include "rgbcol.hpp"

using namespace std;

// Some defaults
const float DEF_LINE_THICKNESS =  1.5;
const int   DEF_LINE_STYLE     =    1;
const float DEF_PTGLYPH_SIZE   =  1.0;
const float DEF_PTGLYPH_PENWID = -1.0;
const float DEF_TXT_SIZE       =  1.0;
const float DEF_TRANS          =  0.3;
const bool  DEF_TXT_BOLDFLAG   = false;

// Some commonly used glyphs for ptgraph
std::map<std::string, const std::string>
	PTGLYPH( 
            {{"Asterisk"    , "#[0x2217]"},
             {"Box"         , "#[0x25A1]"},
             {"BoxDot"      , "#[0x22A1]"},
             {"BoxFBox"     , "#[0x25A3]"},
             {"BoxPlus"     , "#[0x229E]"},
             {"BoxTimes"    , "#[0x22A0]"},
             {"Bspokex"     , "#[0x2723]"},
             {"Circ"        , "#[0x25CB]"},
             {"CircCirc"    , "#[0x25CE]"},
             {"CircDot"     , "#[0x2299]"},
             {"CircFCirc"   , "#[0x25C9]"},
             {"CircPlus"    , "#[0x2295]"},
             {"CircTimes"   , "#[0x2297]"},
             {"Cross"       , "#[0x00D7]"},
             {"CrossHair"   , "#[0x22B9]"},
             {"Diam"        , "#[0x25C7]"},
             {"DiamDot"     , "#[0x27D0]"},
             {"DiamFDiam"   , "#[0x25C8]"},
             {"Dot"         , "#[0x2219]"},
             {"Hex"         , "#[0x2B21]"},
             {"Odot"        , "#[0x29BF]"},
             {"Plus"        , "+"        },
             {"TdropPlus"   , "#[0x2722]"},
             {"ThickCirc"   , "#[0x2B55]"},
             {"TriD"        , "#[0x25BD]"},
             {"TriL"        , "#[0x25C1]"},
             {"TriR"        , "#[0x25B7]"},
             {"TriU"        , "#[0x25B3]"},
             {"TriDot"      , "#[0x25EC]"},
             {"FishEye"     , "#[0x25C9]"},
             {"FillDiam"    , "#[0x25C6]"},
             {"FillBlock"   , "#[0x25A0]"},
             {"FillCirc"    , "#[0x2B59]"},
             {"FillHex"     , "#[0x2B22]"},
             {"FillTriD"    , "#[0x25BC]"},
             {"FillTriL"    , "#[0x25C0]"},
             {"FillTriR"    , "#[0x25B6]"},
             {"FillTriU"    , "#[0x25B2]"}}
            ); 

class sty
{
public:
	inline virtual cpplrgbcol Col()  const { return _col; }
	inline virtual float Thickness() const { return _thickness; }

protected:
	sty(const cpplrgbcol & c, const float & t) { _col = c; _thickness = t; }
	cpplrgbcol _col;
	float      _thickness;
	int        _style;
}; 

class linesty : public sty
{
public:
	inline linesty() : sty((_lcolit == NAMEDCOL.end() ?
	                        _lcolit = NAMEDCOL.begin(), _lcolit->second :
		                    _lcolit->second),
	                       DEF_LINE_THICKNESS)
	{
		_style = ((++deflinestycount)%7 ? deflinestycount%7 : 7);
		_lcolit++;
	}

	inline linesty(const cpplrgbcol & c, const int & s, const float & t)
		: sty(c, t)
	{
		_col = c; _style = s; _thickness = t;
	}

	inline linesty(const linesty & L)
		: sty(L.Col(), L.Thickness())
	{
		_col = L.Col();
		_style = L.Style();
		_thickness = L.Thickness();
	}

	inline int Style()       const { return _style; }
	static std::string ColName()
	{ return (_lcolit != NAMEDCOL.end() ? _lcolit->first
	                                    : NAMEDCOL.begin()->first); }

private:
	static cpplrgbmap::const_iterator _lcolit;
	static unsigned int               deflinestycount;

protected:
	inline static std::pair<string, const cpplrgbcol> LastDefLine()
	{
		cpplrgbmap::const_iterator it = _lcolit;
		return (it==NAMEDCOL.begin() ? *it : *(--it));
	}
};

cpplrgbmap::const_iterator linesty::_lcolit = NAMEDCOL.begin();

unsigned int linesty::deflinestycount = 0;

static const linesty DEF_LINE(BLACK, DEF_LINE_STYLE, DEF_LINE_THICKNESS); 

class ptsty : public sty
{
public:
	ptsty()
		: sty((_ptcolit==NAMEDCOL.end() ?
		       _ptcolit=NAMEDCOL.begin(), _ptcolit->second :
		       _ptcolit->second),
		      DEF_PTGLYPH_PENWID)
	{
		if (_ptglyphit == PTGLYPH.end())
			_ptglyphit  = PTGLYPH.begin();
		_chr       = _ptglyphit->second;
		_size      =  DEF_PTGLYPH_SIZE;
		++_ptglyphit;
		++_ptcolit;
	}

	ptsty(const string & chr,
	      const cpplrgbcol & col,
	      const float & tk,
	      const float & sz)
		: sty(col, tk)
	{
		_chr       = chr;
		_size      = sz;
	}

	ptsty(const ptsty & P) : sty(P.Col(), P.Thickness())
	{
		_chr       = P.Glyph();
		_size      = P.Size();
	}

	inline string Glyph()    const { return _chr; }
	inline float Size()      const { return _size; }
	
	static std::string ColName()
	{ return (_ptcolit != NAMEDCOL.end() ? _ptcolit->first
	                                     : NAMEDCOL.begin()->first); }

	static std::string GlyphName()
	{ return (_ptglyphit != PTGLYPH.end() ? _ptglyphit->first
	                                      : PTGLYPH.begin()->first); }

private:
	string      _chr;
	float       _size;
	static std::map<string, const string>::const_iterator _ptglyphit;
	static cpplrgbmap::const_iterator                     _ptcolit;
};

std::map<string, const string>::const_iterator
	ptsty::_ptglyphit = PTGLYPH.begin();

cpplrgbmap::const_iterator ptsty::_ptcolit = NAMEDCOL.begin();

const ptsty DEF_PT(PTGLYPH["Cross"], BLACK, DEF_PTGLYPH_PENWID,
                   DEF_PTGLYPH_SIZE);

class txtsty : public sty
{
public:
	txtsty() : sty(BLACK, DEF_TXT_SIZE) { _txtjust = 0; }
	txtsty(const cpplrgbcol & c, const float & s = DEF_TXT_SIZE,
	       const float & j = 0)
		: sty(c, s) { _txtjust = j; }
	txtsty(const txtsty & T) : sty(T.Col(), T.Thickness())
	{ _txtjust = T.Just(); }
	inline float Just() const { return _txtjust; }
	inline float Size() const { return this->Thickness(); }
private:
	double _txtjust;
	
};

const txtsty DEF_TXT(BLACK,	DEF_TXT_SIZE, 0);

class fillsty : public sty
{
public:
	fillsty() : sty((_fcolit == NAMEDCOL.end() ?
	                 _fcolit = NAMEDCOL.begin(), _fcolit->second :
		             _fcolit->second),
	                0)
	{ 
		++_fcolit;
		_style = (deffillstycount % 8);
		this->Trans(DEF_TRANS);
		++deffillstycount;
	}

	fillsty(const cpplrgbcol & c, const float & t = 0, const int & s = 0)
		: sty(c, t)
	{
		_style = s;
		this->Trans(DEF_TRANS);
	}

	fillsty(const fillsty & F) : sty(F.Col(), F.Thickness())
	{
		_style = F.Style();
	}

	inline int Style() const { return _style; }
	void Trans(const float & tr)
	{
		if (!validtrans(tr))
			throw transerr;
		this->_col.get<TRANS>() = tr;
	}
	
private:
	static cpplrgbmap::const_iterator _fcolit;
	static unsigned int               deffillstycount;

};

cpplrgbmap::const_iterator fillsty::_fcolit = NAMEDCOL.begin();
unsigned int fillsty::deffillstycount = 0;

const fillsty DEF_AREAFILL(cpplrgbcol(200, 200, 200, 0.5),
                           DEF_LINE_THICKNESS, 0);

class errsty : public linesty
{
public:
	errsty(const bool & ecap = true, const float & th = 1.5)
		: linesty(linesty::LastDefLine().second, 1, th)
	{ _endcaps = true; }

	errsty(const linesty & L, const bool & ec = true)
		: linesty(L.Col(), L.Style(), L.Thickness())
	{ _endcaps = ec; }

	errsty(const errsty & L)
		: linesty(L.Col(), L.Style(), L.Thickness())
	{
		this->_endcaps = L.DrawCaps();
	}

	inline bool DrawCaps() const { return _endcaps; }
	inline linesty LineStyle() const
	{
		return linesty(this->Col(), this->Style(), this->Thickness());
	}

private:
	bool _endcaps;
};

const errsty DEF_ERR (DEF_LINE,	false);

#endif /*SELYTS _H */
