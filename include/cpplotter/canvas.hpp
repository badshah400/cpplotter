/* vim: set cin ts=4 sw=4 tw=80: */
/*
 * cpplcanvas.hpp
 * Copyright (C) 2015 Atri Bhattacharya <badshah400@gmail.com>
 *
 * cpplotter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpplotter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CPPLCANVAS_HPP_
#define _CPPLCANVAS_HPP_

#include <array>

typedef std::array<double, 2> arrd2;

class cpplcanvas 
{
public:
	inline cpplcanvas() { };
	cpplcanvas(const arrd2 & xlims,
	           const arrd2 & ylims,
	           const int axes = 0)
	{
		_xlim = xlims;
		_ylim = ylims;
		_ax   = axes; 
	}

	inline arrd2 x_lims() const { return _xlim; }
	inline arrd2 y_lims() const { return _ylim; }
	inline int axesflag() const { return _ax;   }
	

private:
	 arrd2 _xlim;
	 arrd2 _ylim;
	 int _ax;

};

#endif // _CPPLCANVAS_HPP_

